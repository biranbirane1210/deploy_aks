resource "random_pet" "prefix" {}

provider "azurerm" {
  features {}
}

#resource "azurerm_resource_group" "default" {
#  name     = "${random_pet.prefix.id}-rg"
#  location = "West US 2"
#
#  tags = {
#    environment = "Demo"
#  }
#}

resource "azurerm_kubernetes_cluster" "default" {
  name                = "${random_pet.prefix.id}-aks"
  location            = var.location
  resource_group_name = var.resource_group
  dns_prefix          = "${random_pet.prefix.id}-k8s"

  default_node_pool {
    name            = "default"
    node_count      = 3
    vm_size         = "Standard_B2s"
    os_disk_size_gb = 40
    enable_auto_scaling = true
    max_count = 6
    min_count = 3
  }

  identity {
    type = "SystemAssigned"
  }
  role_based_access_control_enabled = true

  tags = {
    environment = "Demo"
  }
}