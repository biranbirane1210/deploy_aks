#variable "appId" {
#  description = "Azure Kubernetes Service Cluster service principal"
#}
#
#variable "password" {
#  description = "Azure Kubernetes Service Cluster password"
#}

variable "resource_group"{
  description = "Azure resource group in which aks will be deployed"
  default = "Finops-Training-RG"
}

variable "location" {
  default = "westeurope"
}