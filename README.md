# deploy_aks
This repository contains Terraform code that can be
used to deploy an AKS (Azure Kubernetes Service) cluster on the Azure cloud platform. The code is intended to provide a quick and easy way to set up an AKS cluster 
without having to manually configure each component.

## Prerequisites
Before using this code, you will need to have the following:

- An Azure account
- Terraform installed on your local machine
- Azure CLI installed on your local machine

## Usage

To use this code, follow these steps:

1. Clone the repository to your local machine.
2. Open the `terraform.tfvars` file and enter your Azure appId and Password that suppose you have already created a Service Principal
3. Open a command prompt or terminal window and navigate to the directory where the code is stored.
4. Run the following commands to initialize Terraform and deploy the AKS cluster:

````
terraform init
terraform apply
````
Follow the prompts to confirm the deployment and wait for the deployment to complete.
## Conclusion

With this Terraform code, you can easily deploy an AKS cluster on Azure without having to manually configure each component. This can save you time and ensure that your AKS deployment is consistent and reproducible.